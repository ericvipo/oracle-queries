-- USER ERIC
create table prueba (
	prueba_no number(4),
	prueba_nom varchar2(20) not null,
	prueba_01 varchar2(9),
	fecha date,
	prueba_02 number(4),
	constraint pk_prueba primary key (prueba_no),
	constraint fk_prueba_no foreign key (prueba_no) references prueba,
	constraint upper_name check (prueba_nom = upper(prueba_nom))
);

insert into prueba
values(1,'TEST NAME','prueba-1',to_date('09/11/1993','dd/mm/yyyy'),26);

select * from prueba;

-- USER HR
select * from departments;
select * from employees;

-- forma muy redumentaria
-- para hacer una consulta sobre 2 tablas
select
	a.department_id,
	a.department_name,
	b.employee_id,
	b.first_name || ' ' || b.last_name as nombre
from departments a, employees b
where a.department_id = b.department_id;

create sequence nombre_seq
	increment by 1
	start with 1
	maxvalue 100
	nocache
	nocycle;

select * from tab;
select * from prueba;
select * from mitabla;

select to_char(cod1, 'FM000') || ' GAA' from mitabla;

delete from mitabla where cod1 = 1;

insert into mitabla values(nombre_seq.NEXTVAL, 'Eric', 'ericvpoma@gmail.com'); 
insert into mitabla values(nombre_seq.NEXTVAL, 'Antoni', 'eric@gmail.com'); 
insert into mitabla values(nombre_seq.NEXTVAL, 'Antoni', 'eric@gmail.com'); 
insert into mitabla values(nombre_seq.NEXTVAL, 'Antoni'||nombre_seq.NEXTVAL, 'er@gmail.com'); 
insert into mitabla
values(
	nombre_seq.NEXTVAL,
	'ERIC' || '/' || to_char(nombre_seq.NEXTVAL, 'FM000'),
	'helloworld@gmail.com'
);
