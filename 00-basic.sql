-- nombre de tabla debe ser max 30 caracteres
-- AZaz09_

-- TIPOS DE DATOS
-- char(n) | ejemplo char(5) max 255
-- 1 byte por cada caracter
-- varchar2(n) max 2000 bytes

-- number(n,d)
-- importe number(10,2)
-- Entera: 7 digitos
-- Punto decimal: 1 digito
-- Decimal: 2 digitos

-- date (fecha y hora)
-- raw(n) datos en binario max 255 bytes
-- long raw 1xtabla max 2GB

-- CONSTRAINTS
-- pk
-- check validacion en el lado BD
-- check import > 0
-- check upper(nombre)
-- fk
-- REFERENCES, establece y obliga una relacion entre esta
-- columna y la columna de la llave primaria de la tabla referenciada
-- NOT NULL
-- nombre varchar2(30) not null
-- UNIQUE
-- dni varchar2(12) 

select * from departments;
select to_char(department_id, 'FM0000') from departments;
