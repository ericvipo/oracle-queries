--natural join
select * from locations;
select * from countries;
select * from departments;
-- rudimentario
select * from locations, countries
where locations.country_id = countries.country_id;
-- si las tablas estan relacionadas
select * from locations natural join countries;

-- ON clause
-- especifica arbitrariamente las
-- condiciones para el join
select *
from departments d
join employees e
on (e.employee_id = d.department_id);

select
	e.employee_id,
	e.last_name,
	e.department_id
from employees e join departments d
on (e.department_id = d.department_id)
and e.manager_id = 149;

-- self join
select
	worker.last_name as emp,
	mng.last_name as mng
from employees worker join employees mng
on (worker.manager_id = mng.employee_id);

-- nonequijoins
select
	e.last_name,
	e.salary,
	j.job_title
from employees e join jobs j
on e.salary
	between j.min_salary and j.max_salary
	and e.job_id = j.job_id;

-- outer join
-- devolver registros sin coincidencia directa
-- utilizando combinaciones externas
--107
select * from employees;
select * from departments;--27
-- left outer join
select
	e.last_name,
	e.department_id,
	d.department_name
from employees e left outer join departments d
on (e.department_id = d.department_id);
-- right outer join
select
	e.last_name,
	e.department_id,
	d.department_name
from employees e right outer join departments d
on (e.department_id = d.department_id);
-- full outer join
select
	e.last_name,
	e.department_id,
	d.department_name
from employees e full outer join departments d
on (e.department_id = d.department_id);

-- cross join producto cartesiano
select
	last_name,
	department_name
from employees cross join departments;
