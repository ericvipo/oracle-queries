-- Cláusula where
select
	last_name,
	job_id,
	department_id
from employees
where first_name = 'Kelly';

select * from employees
where department_id = 60;

-- comparation operators
-- <>, >, <, >=
-- between value and value
select
	first_name as "Nombre",
	last_name as "Apellido",
	salary as "Salario"
from employees
where salary in (9000, 10000, 11000);

-- like operator
-- busq valida en strings
-- % zero or more chars
-- _ one character
select first_name
from employees
where first_name like 'S%';

select first_name
from employees
where first_name like '_o%';

-- pude haber usado otro símbolo como ESC
select first_name, last_name, job_id
from employees
where job_id like 'SH?_%' escape '?';

-- valores con null
-- is null, is not null
select last_name, manager_id
from employees
where manager_id is null;

-- logic operators
-- or, and, not
select employee_id, last_name, job_id, salary
from employees
where salary >= 10000 and job_id like '%MAN%';

select last_name, job_id
from employees
where job_id
not in ('IT_PROG', 'SA_REP');

/*
* REGLAS DE PRECEDENCIA
* operadores aritmeticos
* operadores de concatenacion
* condicion de comparacion
* is null, like, in
* between
* <>
* condicion not
* condicion and
* condicion or
 */

-- order by
-- para ordenar filas asd desc
select last_name, job_id, department_id, hire_date
from employees
order by hire_date desc;

select
	employee_id,
	last_name,
	salary*12 as annsal
from employees
order by annsal;
--ordena por department_id
select
	last_name, job_id, department_id, hire_date
from employees
order by 3;
-- dos ordenaciones
select
	employee_id,
	last_name,
	department_id,
	salary
from employees
order by department_id, salary desc;
