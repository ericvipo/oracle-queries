-- Select básico
select * from departments;
select * from employees;
-- Proyección
select department_name, location_id from departments;
-- Operaciones aritméticas
select
	last_name, salary, salary + 30
from employees;
-- Toda operación con NULL siempre será NULL
/*
 * Alias de columnas
 * Renombra las cabeceras de columnas
 * Se usa carácteres especiales con doble comilla
 */
select
	last_name as apellido,
	commission_pct comision 
from employees;

select
	last_name as "Apellido",
	salary as "Salario",
	salary*12 "Salario Anual"
from employees;

/*
 * Operador concatenación ||
 * Vincula columnas o carácteres
 * a otra columna
 * El resultado es una columna
*/
select
	last_name || ' is a ' || job_id as "Empleado"
from employees;

-- Quote's use
select
	last_name ||
	q'< Department´s Manager ID: >' ||
	manager_id
	as "Department and Manager"
from employees;

/*
 * Filas duplicadas
 * Omite gracias a DISTINCT
*/
select
	distinct department_id
from employees;

/*
 * Mostrar estructura de una tabla
*/
desc employees;

-- From dual
-- tabla presente en todas
select 1+1 from dual;
select 1 from dual;
select dummy from dual;
select user from dual;
select sysdate from dual;

